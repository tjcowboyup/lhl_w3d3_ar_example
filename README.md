# ActiveRecord Demo

## Naming Conventions
AR conventions require tables to be pluralized and lowercase. `articles`  or `blog_articles`.

AR classes are singular and camel cased. For a table called articles the class will be called Article. camel cased example: `BlogArticle` for a table called `blog_articles`.

## articles table

- title:   string
- content: text
- author:  string

## Validations

http://guides.rubyonrails.org/active_record_validations.html#validation-helpers

## Query Methods

http://guides.rubyonrails.org/active_record_querying.html

## Associations

http://guides.rubyonrails.org/association_basics.html


