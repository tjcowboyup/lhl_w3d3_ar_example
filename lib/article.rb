class Article < ActiveRecord::Base
  belongs_to :author # <-- Convention: name of table (SINGULAR). Needs a column called author_id
  validates :title, presence: true, length: {minimum: 4}

  # .find(id) - return the record with the given id. exception if no record with id.
  # #save - saves the values of the article to the database
  # .new
  def character_count
    content.length
  end

  def to_s
    "#{id} - #{title} by:#{author.name} #{character_count}"
  end
end

# What I want...
# article.author   # SELECT * FROM authors WHERE authors.id = #{article.author_id}
