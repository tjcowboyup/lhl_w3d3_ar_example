class Author < ActiveRecord::Base
  has_many :articles   # <-- Convention: Name of the table
  validates :name, presence: true
end

# It would be really nice if I could do this:
# author.articles  # SELECT * FROM articles WHERE article_id = #{author.id}
