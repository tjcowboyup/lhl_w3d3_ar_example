require 'pry' # in case you want to use binding.pry
require 'active_record'
require_relative 'lib/article'
require_relative 'lib/author'

# Output messages from Active Record to standard out
ActiveRecord::Base.logger = Logger.new(STDOUT)

puts 'Establishing connection to database ...'
ActiveRecord::Base.establish_connection(
  adapter: 'postgresql',
  database: 'ar_exercises',
  username: 'monica',
  password: '',
  host: 'localhost',
  port: 5432,
  pool: 5,
  encoding: 'unicode',
  min_messages: 'error'
)
puts 'CONNECTED'

puts 'Setting up Database (recreating tables) ...'

ActiveRecord::Schema.define do
  drop_table :articles if ActiveRecord::Base.connection.table_exists?(:articles)
  drop_table :authors if ActiveRecord::Base.connection.table_exists?(:authors)
  create_table :articles do |t|
    t.column :title, :string
    t.column :content, :text
    t.column :author_id, :integer   # Convention: TABLENAME(SINGULAR)_id
    t.timestamps null: false
  end
  create_table :authors do |t|
    t.column :name, :string
    t.timestamps null: false # created_at updated_at
  end
end

puts 'Setup DONE'

# Create authors
daffy = Author.create(name: "Daffy Duck")
elmer = Author.create(name: "Elmer")

# Create some articles
article = Article.new
article.title = "Ducks are awesome"
article.content = "hello"
article.author  = daffy
article.save # take all attributes and write them to a row in the database

if !article.persisted?
  puts "Could not save article with title #{article.title}"
  article.errors.messages.each do |column, messages|
    puts "#{column.capitalize}"
    messages.each do |message|
      puts "  - #{message}"
    end
  end
end

# Article.create(title: "Ducks are delicious", content: "boo ya", author: elmer)
elmer.articles.create(title: "Ducks are delicious", content: "boo ya")

# new creates an instance of article, but does not save
# article3 = Article.new(title: "Hunters suck", content: "bar", author: "Daffy Duck")
# article3.save
article3 = daffy.articles.build(title: "Hunters suck", content: "bar")
article3.save

puts "All articles"
Article.all.each do |article|
  puts "   #{article}"
end

